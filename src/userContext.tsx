import { createContext } from "solid-js";

interface IUserContextState {
  isAdmin: boolean;
}

export const UserContext = createContext<IUserContextState>({ isAdmin: false });

export const UserContextProvider = (props: { children: JSX.Element }) => {
  const params = new URL(window.location.href).searchParams;
  const isAdmin = params.get("isAdmin"); // is the string "Jonathan Smith".
  return (
    <UserContext.Provider
      value={{ isAdmin: isAdmin === "1" || isAdmin === 1 ? true : false }}
    >
      {props.children}
    </UserContext.Provider>
  );
};
