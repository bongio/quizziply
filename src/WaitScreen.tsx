import {BaseScreen} from './styledComponents/BaseScreen'

export const WaitScreen = () => (
  <BaseScreen>
    <h2 className="mt-10 mx-4 text-green-800">
      {'Waiting for the next step in the quiz...'}
    </h2>
  </BaseScreen>
)
