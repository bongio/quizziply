interface IActionButtonProps {
  children?: JSX.Element;
  label?: string;
  onClick?: (e: MouseEvent) => void;
}

export const ActionButton = (props: IActionButtonProps) => (
  <button
    className="bg-blue-700 text-white hover:bg-blue-900 hover:text-gray-100 hover:cursor-pointer px-8 py-4 text-lg rounded border-0 outline-none"
    onClick={props.onClick}
    type="button"
    >
    {props.children || props.label || 'button'}
  </button>
);
