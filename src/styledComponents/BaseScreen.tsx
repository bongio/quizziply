export const BaseScreen = (props: {children: JSX.Element, label?: string}) => (
  <>
    <div className="w-full max-w-full px-4 md:px-8 lg:px-16 w-full py-2 md:py-4 flex-shrink-0 flex-grow-0 text-center bg-blue-800 text-white">
      <h1>{props.label || "Starfishes Quiz 4"}</h1>
    </div>
    {props.children}
  </>
);
