import { useQuizData } from "./quizData";
import { QuizStateContext } from "./QuizStateContext";
import { useContext } from "solid-js";
import { BaseScreen } from "./styledComponents/BaseScreen";
import {ActionButton} from './styledComponents/ActionButton'

export const WelcomeScreen = () => {
  const quizData = useQuizData();
  const { authorName, quizName } = quizData;
  const [_, { onNextStep }] = useContext(QuizStateContext);

  return (
    <BaseScreen>
      <h1 className="px-8 mt-10 text-gray-800 text-center">{`Welcome to the ${quizName}`}</h1>
      <div className="flex justify-center">
        <ActionButton onClick={onNextStep}>{'Start'}</ActionButton>
      </div>
    </BaseScreen>
  );
};
