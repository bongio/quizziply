import localStore from "store";
import { IQuizData, IQuizQuestion, useQuizData } from "./quizData";
import {
  createContext,
  createEffect, createMemo,
  createState,
  onCleanup,
  unwrap,
  useContext
} from 'solid-js'
import { UserContext, UserContextProvider } from "./userContext";

export enum IQuizStatus {
  NOT_STARTED,
  PLAYING,
  REVIEWING,
  COMPLETED,
}

interface IQuizState {
  status: IQuizStatus;
  currentTurn: number;
  currentQuestion: number;
}

interface IPersonalQuizProgress {
  personal: IQuizState;
  myAnswers: {
    [questionId: string]: number;
  };
}

interface IQuizProgress extends IPersonalQuizProgress {
  shared: IQuizState;
}

interface IQuizProgressHandlers {
  onNextStep: () => void;
  onPreviousStep: () => void;
  onAnswerQuestion: (
    turnId: number,
    questionId: number,
    answerId: number
  ) => void;
}

type BooleanMemo = () => boolean;
type IQuizProgressContext = [IPersonalQuizProgress, IQuizProgressHandlers, BooleanMemo];

const initialQuizProgress: () => IQuizProgress = () => ({
  shared: {
    status: IQuizStatus.NOT_STARTED,
    currentTurn: 0,
    currentQuestion: 0,
  },
  personal: {
    currentQuestion: 0,
    currentTurn: 0,
    status: IQuizStatus.NOT_STARTED,
  },
  myAnswers: {},
});

function _sanitizeQuizState(
  { turns }: IQuizData,
  progress: IQuizState
): IQuizState {
  const { status, currentTurn, currentQuestion } = progress;
  // 1. Sanity check on quiz data and indexes for turn and question
  if (
    !turns ||
    !Array.isArray(turns) ||
    turns.length < 0 ||
    !(status in IQuizStatus)
  ) {
    return "myAnswers" in progress
      ? {
          status: IQuizStatus.COMPLETED,
          currentQuestion: 0,
          currentTurn: 0,
        }
      : {
          status: IQuizStatus.COMPLETED,
          currentQuestion: 0,
          currentTurn: 0,
        };
  }
  if (turns.some((turn) => turn.questions.length < 1)) {
    return "myAnswers" in progress
      ? {
          status: IQuizStatus.COMPLETED,
          currentQuestion: 0,
          currentTurn: 0,
        }
      : {
          status: IQuizStatus.COMPLETED,
          currentQuestion: 0,
          currentTurn: 0,
        };
  }
  const nTurn = currentTurn < 0 ? 0 : Math.min(turns.length - 1, currentTurn);
  const turn = turns[nTurn];
  const nQuestion =
    turns.length < 1 ? 0 : Math.min(turn.questions.length - 1, currentQuestion);
  return "myAnswers" in progress
    ? {
        status,
        currentTurn: nTurn,
        currentQuestion: nQuestion,
      }
    : {
        status,
        currentTurn: nTurn,
        currentQuestion: nQuestion,
      };
}

export function previousStep(
  quizData: IQuizData,
  quizState: IQuizState
): IQuizState {
  const { currentTurn, currentQuestion, status } = _sanitizeQuizState(
    quizData,
    quizState
  );
  const { turns } = quizData;

  // 1. Sanity check on quiz data and indexes for turn and question
  if (turns.length < 0) {
    return {
      status: IQuizStatus.COMPLETED,
      currentQuestion: 0,
      currentTurn: 0,
    };
  }
  if (turns.some((turn) => turn.questions.length < 1)) {
    return {
      status: IQuizStatus.COMPLETED,
      currentQuestion: 0,
      currentTurn: 0,
    };
  }
  const nTurn = Math.max(0, Math.min(turns.length - 1, currentTurn));
  const turn = turns[nTurn];
  const nQuestion = Math.max(
    0,
    Math.min(turn.questions.length - 1, currentQuestion)
  );
  if (status === IQuizStatus.NOT_STARTED) {
    return quizState;
  }
  if (
    status === IQuizStatus.PLAYING ||
    status === IQuizStatus.REVIEWING ||
    status === IQuizStatus.COMPLETED
  ) {
    if (status === IQuizStatus.COMPLETED || (nTurn === 0 && nQuestion === 0)) {
      return {
        status:
          status === IQuizStatus.PLAYING
            ? IQuizStatus.NOT_STARTED
            : status === IQuizStatus.REVIEWING
            ? IQuizStatus.PLAYING
            : IQuizStatus.REVIEWING,
        currentQuestion:
          status === IQuizStatus.PLAYING
            ? 0
            : turns[turns.length - 1].questions.length - 1,
        currentTurn: status === IQuizStatus.PLAYING ? 0 : turns.length - 1,
      };
    }
    if (nQuestion > 0) {
      return {
        status,
        currentTurn: nTurn,
        currentQuestion: nQuestion - 1,
      };
    }
    const previousTurn = turns[nTurn - 1];
    return {
      status,
      currentTurn: nTurn - 1,
      currentQuestion: previousTurn.questions.length - 1,
    };
  }
  return {
    status: IQuizStatus.COMPLETED,
    currentQuestion: 0,
    currentTurn: 0,
  };
}

/**
 * Given the quiz setup in quizData, and a progress object,
 * return the step that will follow the current state
 * @param {IQuizData} quizData
 * @param {IQuizState} quizState
 * @returns {IQuizState}
 */
export function nextStep<T extends IQuizState>(
  quizData: IQuizData,
  quizState: T
): IQuizState {
  const { currentTurn, currentQuestion, status } = _sanitizeQuizState(
    quizData,
    quizState
  );
  const { turns } = quizData;

  // 1. Sanity check on quiz data and indexes for turn and question
  if (turns.length < 0) {
    return {
      status: IQuizStatus.COMPLETED,
      currentQuestion: 0,
      currentTurn: 0,
    };
  }
  if (turns.some((turn) => turn.questions.length < 1)) {
    return {
      status: IQuizStatus.COMPLETED,
      currentQuestion: 0,
      currentTurn: 0,
    };
  }
  const nTurn = currentTurn < 0 ? 0 : Math.min(turns.length - 1, currentTurn);
  const turn = turns[nTurn];
  const nQuestion =
    turns.length < 1 ? 0 : Math.min(turn.questions.length - 1, currentQuestion);
  if (status === IQuizStatus.NOT_STARTED) {
    return {
      status: IQuizStatus.PLAYING,
      currentQuestion: 0,
      currentTurn: 0,
    };
  }
  if (status === IQuizStatus.PLAYING) {
    if (nQuestion < turn.questions.length - 1) {
      return {
        status: IQuizStatus.PLAYING,
        currentTurn,
        currentQuestion: currentQuestion + 1,
      };
    } else {
      return nTurn < turns.length - 1
        ? {
            status: IQuizStatus.PLAYING,
            currentTurn: nTurn + 1,
            currentQuestion: 0,
          }
        : {
            status: IQuizStatus.REVIEWING,
            currentTurn: 0,
            currentQuestion: 0,
          };
    }
  }
  if (status === IQuizStatus.REVIEWING) {
    if (nQuestion < turn.questions.length - 1) {
      return {
        status: IQuizStatus.REVIEWING,
        currentTurn,
        currentQuestion: currentQuestion + 1,
      };
    } else {
      return nTurn < turns.length - 1
        ? {
            status: IQuizStatus.REVIEWING,
            currentTurn: nTurn + 1,
            currentQuestion: 0,
          }
        : {
            status: IQuizStatus.COMPLETED,
            currentTurn: 0,
            currentQuestion: 0,
          };
    }
  }
  return {
    status: IQuizStatus.COMPLETED,
    currentTurn: 0,
    currentQuestion: 0,
  };
}

function targetProgress(
  quizData: IQuizData,
  shared: IQuizState,
  personal: IQuizState
): IQuizState {
  if (quizData.turns.length < 0) {
    return {
      status: IQuizStatus.COMPLETED,
      currentQuestion: 0,
      currentTurn: 0,
    };
  }
  if (quizData.turns.some((turn) => turn.questions.length < 1)) {
    return {
      status: IQuizStatus.COMPLETED,
      currentQuestion: 0,
      currentTurn: 0,
    };
  }
  const sharedProgress = _sanitizeQuizState(quizData, shared);
  const personalProgress = _sanitizeQuizState(quizData, personal);
  if (personalProgress.status > sharedProgress.status) {
    return nextStep(quizData, sharedProgress);
  }
  if (personalProgress.status < sharedProgress.status) {
    return personalProgress;
  }
  if (
    personalProgress.status === IQuizStatus.PLAYING ||
    personalProgress.status === IQuizStatus.REVIEWING
  ) {
    if (personalProgress.currentTurn < sharedProgress.currentTurn) {
      return personalProgress;
    }
    if (
      personalProgress.currentTurn === sharedProgress.currentTurn &&
      personalProgress.currentQuestion <= sharedProgress.currentQuestion
    ) {
      return personalProgress;
    }
    return nextStep(quizData, sharedProgress);
  }
  return personalProgress;
}

const showWaitingScreen = (
  sharedState: IQuizState,
  myState: IQuizState
): boolean => {
  if (sharedState.status < myState.status) {
    return true;
  }
  if (sharedState.status > myState.status) {
    return false;
  }
  if (
    sharedState.status === IQuizStatus.COMPLETED ||
    sharedState.status === IQuizStatus.NOT_STARTED
  ) {
    return false;
  }
  if (sharedState.currentTurn < myState.currentTurn) {
    return true;
  }
  if (sharedState.currentTurn > myState.currentTurn) {
    return false;
  }
  return Boolean(sharedState.currentQuestion < myState.currentQuestion);
};

export const QuizStateContext = createContext<IQuizProgressContext>();
const quizProgressStorageKey = "starfishQuizProgress";

export const QuizProgressProvider = (props: { children: JSX.Element }) => {
  const savedProgress: Partial<IPersonalQuizProgress> =
    localStore.get(quizProgressStorageKey) || {};
  let initialProgress = initialQuizProgress();
  initialProgress = {
    ...initialProgress,
    personal: {
      ...initialProgress.personal,
      ...(savedProgress.personal || {}),
    },
    myAnswers: {
      ...initialProgress.myAnswers,
      ...(savedProgress.myAnswers || {}),
    },
  };
  console.log(`Initial progress:`);
  console.log(initialProgress);
  const [quizProgress, setQuizProgress] = createState<IQuizProgress>(
    initialProgress
  );
  const [combinedProgress, setCombinedProgress] = createState<
    IPersonalQuizProgress
  >({ personal: quizProgress.shared, myAnswers: quizProgress.myAnswers });
  const shouldShowWaitingScreen = createMemo(() => showWaitingScreen(quizProgress.shared, quizProgress.personal));
  const quizData = useQuizData();
  const userData = useContext(UserContext);
  createEffect(() => {
    const updatedProgress = {
      personal: targetProgress(
        quizData,
        quizProgress.shared,
        quizProgress.personal
      ),
      myAnswers: quizProgress.myAnswers,
    };
    console.log("Updating combined progress");
    console.log(updatedProgress);
    setCombinedProgress(updatedProgress);
  });
  if (!userData.isAdmin) {
    const intervalId = setInterval(async () => {
      try {
        const sharedProgressData: IQuizState = await fetch(
          "/api/progress"
        ).then((response) => response.json());
        if (sharedProgressData) {
          setQuizProgress("shared", sharedProgressData);
        }
      } catch (err) {
        console.log("Error while refreshing shared progress data");
        console.log(err);
      }
    }, 5000);
    onCleanup(() => clearInterval(intervalId));
  }

  const store: IQuizProgressContext = [
    combinedProgress,
    {
      onNextStep: () => {
        setQuizProgress((quizContextProgress) => {
          const updatedProgress: {
            personal: IQuizState;
            shared?: IQuizState;
          } = {
            personal: targetProgress(
              quizData,
              quizContextProgress.shared,
              nextStep(quizData, quizContextProgress.personal)
            ),
          };
          if (userData.isAdmin) {
            updatedProgress.shared = {
              status: updatedProgress.personal.status,
              currentQuestion: updatedProgress.personal.currentQuestion,
              currentTurn: updatedProgress.personal.currentTurn,
            };
            fetch("/api/progress", {
              method: "POST",
              cache: "no-cache",
              credentials: "same-origin",
              headers: {
                "Content-Type": "application/json",
              },
              body: JSON.stringify(updatedProgress.shared),
            })
              .then(() => {})
              .catch(() => {
                console.log("Error on saving progress");
              });
          }
          localStore.set(quizProgressStorageKey, {
            personal: updatedProgress.personal,
            myAnswers: quizContextProgress.myAnswers,
          });
          return updatedProgress;
        });
      },
      onPreviousStep: () => {
        setQuizProgress((quizContextProgress) => {
          const updatedProgress: {
            personal: IQuizState;
            shared?: IQuizState;
          } = {
            personal: targetProgress(
              quizData,
              quizContextProgress.shared,
              previousStep(quizData, quizContextProgress.personal)
            ),
          };
          if (userData.isAdmin) {
            updatedProgress.shared = {
              status: updatedProgress.personal.status,
              currentQuestion: updatedProgress.personal.currentQuestion,
              currentTurn: updatedProgress.personal.currentTurn,
            };
            fetch("/api/progress", {
              method: "POST",
              cache: "no-cache",
              credentials: "same-origin",
              headers: {
                "Content-Type": "application/json",
              },
              body: JSON.stringify(updatedProgress.shared),
            })
              .then(() => {})
              .catch(() => {
                console.log("Error on saving progress");
              });
          }
          localStore.set(quizProgressStorageKey, {
            personal: updatedProgress.personal,
            myAnswers: quizContextProgress.myAnswers,
          });
          return updatedProgress;
        });
      },
      onAnswerQuestion: (
        turnId: number,
        questionId: number,
        answerId: number
      ) => {
        setQuizProgress((quizContextProgress) => {
          quizContextProgress.personal = {
            ...targetProgress(
              quizData,
              quizContextProgress.shared,
              nextStep(quizData, quizContextProgress.personal)
            ),
          };
          quizContextProgress.myAnswers[
            idForQuestion(turnId, questionId)
          ] = answerId;
          localStore.set(quizProgressStorageKey, {
            personal: quizContextProgress.personal,
            myAnswers: quizContextProgress.myAnswers,
          });
          if (userData.isAdmin) {
            quizContextProgress.shared = {
              status: quizContextProgress.personal.status,
              currentQuestion: quizContextProgress.personal.currentQuestion,
              currentTurn: quizContextProgress.personal.currentTurn,
            };
            fetch("/api/progress", {
              method: "POST",
              cache: "no-cache",
              credentials: "same-origin",
              headers: {
                "Content-Type": "application/json",
              },
              body: JSON.stringify(quizContextProgress.shared),
            })
              .then(() => {})
              .catch(() => {
                console.log("Error on saving progress");
              });
          }
          console.log("Saving updated progress:");
          console.log(quizContextProgress);
        });
      },
    },
    shouldShowWaitingScreen
  ];
  return (
    <QuizStateContext.Provider value={store}>
      {props.children}
    </QuizStateContext.Provider>
  );
};

export const idForQuestion = (turnId: number, questionId: number): string =>
  `T${turnId}.Q${questionId}`;
