import { createDependentEffect, createState } from "solid-js";

interface IRevealingImageProps {
  finalOpacity?: number;
  initialOpacity?: number;
  href: string;
}

export const RevealingImage = (props: IRevealingImageProps) => {
  const { initialOpacity = 0.95, finalOpacity = 0 } = props;
  const [style, setStyle] = createState({ opacity: initialOpacity });
  const onDecreaseCoverOpacity = () => {
    const targetOpacity = Math.max(0, finalOpacity);
    if (style.opacity > targetOpacity) {
      setStyle("opacity", Math.max(targetOpacity, style.opacity - 0.1));
    }
  };
  createDependentEffect(
    () => {
      setStyle("opacity", initialOpacity);
    },
    () => [props.href]
  );
  return (
    <div className="relative w-full max-w-xl" onclick={onDecreaseCoverOpacity}>
      <img src={props.href} class="w-full h-auto" />
      <div
        className="absolute inset-0 bg-gray-900"
        style={{ opacity: `${style.opacity}` }}
      />
    </div>
  );
};
