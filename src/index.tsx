import { render } from "solid-js/dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { QuizProgressProvider } from "./QuizStateContext";
import { Routing } from "./Routing";
import { QuizDataProvider } from "./quizData";
import { UserContextProvider } from "./userContext";

render(
  () => (
    <UserContextProvider>
      <QuizDataProvider>
        <QuizProgressProvider>
          <App />
        </QuizProgressProvider>
      </QuizDataProvider>
    </UserContextProvider>
  ),
  document.getElementById("root") as Node
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
