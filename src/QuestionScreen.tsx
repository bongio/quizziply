import { For } from "solid-js/dom";
import { idForQuestion, nextStep, QuizStateContext } from "./QuizStateContext";
import { useQuizData } from "./quizData";
import {
  createDependentEffect,
  createMemo,
  createState,
  unwrap,
  useContext,
} from "solid-js";
import { ActionButton } from "./styledComponents/ActionButton";
import { RevealingImage } from "./RevalingImage";
import { BaseScreen } from "./styledComponents/BaseScreen";

export const QuestionScreen = () => {
  const [progress, { onAnswerQuestion, onPreviousStep }] = useContext(
    QuizStateContext
  );
  const quizData = useQuizData();
  const question = createMemo(() => {
    const { turns } = quizData;
    const turn = turns[progress.personal.currentTurn];
    return turn.questions[progress.personal.currentQuestion];
  }, quizData.turns[progress.personal.currentTurn].questions[progress.personal.currentQuestion]);
  const [answer, setAnswer] = createState({ index: -1, clickedNext: false });
  createDependentEffect(
    () => {
      const questionId = idForQuestion(
        progress.personal.currentTurn,
        progress.personal.currentQuestion
      );
      const myAnswers = unwrap(progress.myAnswers);
      if (questionId in myAnswers) {
        setAnswer({ index: myAnswers[questionId] });
      } else {
        setAnswer({ index: -1, clickedNext: false });
      }
    },
    () => [progress.personal.currentTurn, progress.personal.currentQuestion],
    false
  );
  const initialAnswers = question().answers.map((answerOption, index) => ({
    ...answerOption,
    id: index,
    isSelected: index === answer.index,
  }));
  const answers = createMemo(
    () =>
      question().answers.map((answerOption, index) => ({
        ...answerOption,
        id: index,
        isSelected: index === answer.index,
      })),
    initialAnswers
  );

  return (
    <BaseScreen>
      <div className="flex w-full max-w-full px-8 py-3 justify-between text-sm text-blue-900">
        <div className="flex-none">
          {`Turn ${progress.personal.currentTurn + 1} / ${quizData.turns.length}`}
        </div>
        <div className="flex-none">
          {`Question ${progress.personal.currentQuestion + 1} / ${quizData.turns[progress.personal.currentTurn].questions.length}`}
        </div>
      </div>
      <div className="px-8 md:px-8 lg:px-16 w-full max-w-full py-2 md:py-4 flex-shrink-0 flex-grow-0 text-gray-800">
        <h3>{question().question}</h3>
      </div>
      {question().img && (
        <div className="px-4 md:px-8 lg:px-16 w-full max-w-full py-2 md:py-4 flex-shrink-0 flex-grow-0 flex flex-row justify-center">
          <RevealingImage href={question().img!} />
        </div>
      )}
      <div className="px-4 md:px-8 lg:px-16 w-full py-4 md:py-8 flex-1 flex flex-col items-center items-center">
        <For each={answers()}>
          {(answerOption) => (
            <button
              type="button"
              className={`border border-gray-300 border-solid outline-none hover:cursor-pointer hover:bg-gray-200 w-full max-w-sm py-4 px-4 my-4 select-none rounded shadow${
                answerOption.isSelected ? " bg-green-300" : ""
              }`}
              onClick={() => {
                setAnswer({ index: answerOption.id });
              }}
            >
              {answerOption.text}
            </button>
          )}
        </For>
      </div>
      <div
        className={`px-4 md:px-8 lg:px-16 py-4 md:py-8 ${
          answer.clickedNext && answer.index === -1 ? "visible" : "invisible"
        } text-red-700 flex flex-col max-w-3xl w-full items-center`}
      >
        {"Please select one of the options above"}
      </div>
      <div className="w-full max-w-full px-4 md:px-8 lg:px-16 py-4 md:py-8 flex justify-between items-center">
        <ActionButton onClick={onPreviousStep}>{"Back"}</ActionButton>
        <ActionButton
          onClick={() => {
            if (answer.index !== -1) {
              onAnswerQuestion(
                progress.personal.currentTurn,
                progress.personal.currentQuestion,
                answer.index
              );
            } else {
              setAnswer({ clickedNext: true });
            }
          }}
        >
          {"Next"}
        </ActionButton>
      </div>
    </BaseScreen>
  );
};
