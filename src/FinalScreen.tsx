import { BaseScreen } from "./styledComponents/BaseScreen";
import { ActionButton } from "./styledComponents/ActionButton";
import { useContext } from "solid-js";
import { QuizStateContext } from "./QuizStateContext";

export const FinalScreen = () => {
  const [_, { onPreviousStep }] = useContext(QuizStateContext);
  return (
    <BaseScreen>
      <div className="flex-grow">
        <h2 className="mt-10 mx-4 text-green-800">
          {"And we are done for this week, thanks for coming!"}
        </h2>
      </div>
      <div className="w-full max-w-full px-4 md:px-8 lg:px-16 py-4 md:py-8 flex justify-between items-center">
        <ActionButton onClick={onPreviousStep}>{"Back"}</ActionButton>
      </div>
    </BaseScreen>
  );
};
