import { Match, Switch } from "solid-js/dom";
import { IQuizStatus, QuizStateContext } from "./QuizStateContext";
import { WelcomeScreen } from "./Welcome";
import { useContext } from "solid-js";
import { QuestionScreen } from "./QuestionScreen";
import { WaitScreen } from "./WaitScreen";
import { UserContext } from "./userContext";
import { ReviewScreen } from "./ReviewScreen";
import { FinalScreen } from "./FinalScreen";

export const Routing = () => {
  const [progress, _, showWaitingScreen] = useContext(QuizStateContext);
  const user = useContext(UserContext);
  return (
    <Switch fallback={WelcomeScreen}>
      <Match when={!user.isAdmin && showWaitingScreen()}>
        <WaitScreen />
      </Match>
      <Match when={progress.personal.status === IQuizStatus.NOT_STARTED}>
        <WelcomeScreen />
      </Match>
      <Match when={progress.personal.status === IQuizStatus.PLAYING}>
        <QuestionScreen />
      </Match>
      <Match when={progress.personal.status === IQuizStatus.REVIEWING}>
        <ReviewScreen />
      </Match>
      <Match when={progress.personal.status === IQuizStatus.COMPLETED}>
        <FinalScreen />
      </Match>
    </Switch>
  );
};
