import {createEffect, createMemo, createState, useContext} from 'solid-js'
import { idForQuestion, QuizStateContext } from "./QuizStateContext";
import { IQuizQuestion, useQuizData } from "./quizData";
import { BaseScreen } from "./styledComponents/BaseScreen";
import { RevealingImage } from "./RevalingImage";
import { ActionButton } from "./styledComponents/ActionButton";

function reviewAnswer(
  turnId: number,
  questionId: number,
  question: IQuizQuestion,
  answers: { [qId: string]: number }
): {
  correctAnswer: string;
  yourAnswer: string;
} {
  const qId = idForQuestion(turnId, questionId);
  const answerIndex = qId in answers ? answers[qId] : -1;
  const correctIndex = question.answers.findIndex(
    (answer) => answer.isCorrectAnswer
  );
  return {
    correctAnswer: question.answers[correctIndex].text,
    yourAnswer: answerIndex >= 0 ? question.answers[answerIndex].text : "",
  };
}

export const ReviewScreen = () => {
  const [progress, { onPreviousStep, onNextStep }] = useContext(
    QuizStateContext
  );
  const quizData = useQuizData();
  const [answerResult, setAnswerResult] = createState({
    ...reviewAnswer(
      progress.personal.currentTurn,
      progress.personal.currentQuestion,
      quizData.turns[progress.personal.currentTurn].questions[
        progress.personal.currentQuestion
      ],
      progress.myAnswers
    ),
    question:
      quizData.turns[progress.personal.currentTurn].questions[
        progress.personal.currentQuestion
      ],
  });
  createEffect(() => {
    const {turns} = quizData;
    const turn = turns[progress.personal.currentTurn];
    const question = turn.questions[progress.personal.currentQuestion];
    setAnswerResult({
      ...reviewAnswer(
        progress.personal.currentTurn,
        progress.personal.currentQuestion,
        question,
        progress.myAnswers
      ),
      question,
    });
  });
  return (
    <BaseScreen label="Review quiz">
      <div className="flex w-full max-w-full px-8 py-3 justify-between text-sm text-blue-900">
        <div className="flex-none">
          {`Turn ${progress.personal.currentTurn + 1} / ${
            quizData.turns.length
          }`}
        </div>
        <div className="flex-none">
          {`Question ${progress.personal.currentQuestion + 1} / ${
            quizData.turns[progress.personal.currentTurn].questions.length
          }`}
        </div>
      </div>
      <div className="px-8 md:px-8 lg:px-16 w-full max-w-full py-2 md:py-4 flex-shrink-0 flex-grow-0 text-gray-800">
        <h3>{answerResult.question.question}</h3>
      </div>
      {answerResult.question.img && (
        <div className="px-4 md:px-8 lg:px-16 w-full max-w-full py-2 md:py-4 flex-shrink-0 flex-grow-0 flex flex-row justify-center">
          <RevealingImage
            href={answerResult.question.img!}
            initialOpacity={0}
            finalOpacity={0}
          />
        </div>
      )}
      <div className="px-8 md:px-8 lg:px-16 w-full max-w-full py-2 md:py-4 flex-shrink-0 flex-grow-0 text-gray-800 flex flex-col items-center">
        <p>{"You chose"}</p>
        <p className="font-bold">{answerResult.yourAnswer}</p>
        {answerResult.yourAnswer === answerResult.correctAnswer ? (
          <p>{"and it was the right answer"}</p>
        ) : (
          <p>{`the right answer was: ${answerResult.correctAnswer}`}</p>
        )}
      </div>
      <div className="w-full max-w-full px-4 md:px-8 lg:px-16 py-4 md:py-8 flex justify-between items-center">
        <ActionButton onClick={onPreviousStep}>{"Back"}</ActionButton>
        <ActionButton onClick={onNextStep}>{"Next"}</ActionButton>
      </div>
    </BaseScreen>
  );
};
