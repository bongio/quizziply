import {createContext, createState, onCleanup, useContext} from 'solid-js'

interface IQuizAnswer {
  text: string;
  img?: string;
  isCorrectAnswer: boolean;
}

export interface IQuizQuestion {
  question: string;
  img?: string;
  answers: IQuizAnswer[];
}

export interface IQuizTurn {
  name: string;
  questions: IQuizQuestion[];
}

export interface IQuizData {
  quizName: string;
  authorName: string;
  authorEmail: string;
  turns: IQuizTurn[];
}

const QuizDataContext = createContext<IQuizData>();

const testQuiz: IQuizData = {
  authorEmail: "pfbongio@gmail.com",
  authorName: "Paolo & Arianna",
  turns: [
    {
      name: "First turn",
      questions: [
        {
          question: "What is that you can eat and obtain with red and yellow?",
          answers: [
            { text: "Orange", isCorrectAnswer: true },
            { text: "Pizza", isCorrectAnswer: false },
            { text: "Strawberry", isCorrectAnswer: false },
            { text: "Pepper", isCorrectAnswer: false },
          ],
        },
        {
          question: "What kind of pasta is all twirly?",
          answers: [
            { text: "Penne", isCorrectAnswer: true },
            { text: "Spaghetti", isCorrectAnswer: false },
            { text: "Fusilli", isCorrectAnswer: false },
            { text: "Rigatoni", isCorrectAnswer: false },
          ],
        }
      ],
    },
  ],
  quizName: "Downs Infant Q4",
};

export const QuizDataProvider = (props: { children: JSX.Element }) => {
  const [quizData, setQuizData] = createState(testQuiz);
  const refreshQuizData = () => {
    fetch('/quizData.json').then(res => res.json()).then(data => setQuizData(data)).catch(console.log);
  }
  refreshQuizData()
  const timerId = setInterval(refreshQuizData, 60000)
  onCleanup(() => clearInterval(timerId));
  return (
    <QuizDataContext.Provider value={quizData}>
      {props.children}
    </QuizDataContext.Provider>
  );
}

export const useQuizData = () => useContext(QuizDataContext);
