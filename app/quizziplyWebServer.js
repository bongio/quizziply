const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const app = express();
const storage = require('node-persist');

const quizDataKey = 'starfishQuizData';
const quizProgressKey = 'starfishQuizSharedProgress';

app.use('/api', bodyParser.json());

app.get('/api/progress', async (req, res) => {
  const savedProgress = await storage.getItem(quizProgressKey)
  res.type('application/json');
  res.send(JSON.stringify(savedProgress || null))
});

app.post('/api/progress', async (req, res) => {
  res.type('application/json');
  try {
    await storage.setItem(quizProgressKey, req.body)
    res.send(JSON.stringify({result: 'ok'}));
  } catch (err) {
    res.send(JSON.stringify({result: 'error'}));
  }
});

app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, '../build', 'index.html'));
});

app.use(express.static(path.join(__dirname, '../build')));

storage.init({dir: 'db'});
app.listen(80);

